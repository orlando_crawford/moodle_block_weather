<?php
$tasks = array(
    array(
        'classname' => 'block_weather\tasks\query_weather_data',
        'blocking' => 0,
        'minute' => '*', //*/1
        'hour' => '*',
        'day' => '*',
        'dayofweek' => '*', //-1-6
        'month' => '*'
    )
);
