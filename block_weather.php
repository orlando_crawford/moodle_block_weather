<?php
require_once(__DIR__.'/../../config.php');
function print_array($val) : string{
  echo '<pre>' . print_r($val) . '</pre>';
}
class enum_task_error{
  const OK = 0;
  const FAILED_TO_INIT_CURL = 1;
  const FAILED_TO_EXECUTE_CURL = 2;
  const BAD_RESPONSE = 3;
  const REQUEST_BAD_KEY = 4;
  const REQUEST_BAD_ARGS = 5;
  const FAILED_TO_PARSE_DATA = 6;
  const FAILED_TO_WRITE_DB = 7;
  const FAILED_TO_READ_DB = 8;
};
$error_table = array(
  "OK",
  "FAILED_TO_INIT_CURL",
  "FAILED_TO_EXECUTE_CURL",
  "BAD_RESPONSE",
  "REQUEST_BAD_KEY",
  "REQUEST_BAD_ARGS",
  "FAILED_TO_PARSE_DATA"
);
function get_temp_chart($int_level) : string{
}
function get_windspeed_chart($int_rot) : string{
  return '</p><div class=\'chart-container\'>
            <div class=\'chart-circle\'>
              <div class=\'chart-bar\' rot=\''. $int_rot. '\' style=\'transform: rotate(' . '0' . 'deg) !important;\'></div>
            </div>
          </div>';
}
function get_buttons() : string {
  return '<div id="weather-controls-info" class = "weather-controls">
            <div id = "weather-left" class = "weather-button weather-button-left">&#9668;</div>
            <div id = "weather-info-center" class="weather-button-center">.</div>
            <div id = "weather-right" class = "weather-button weather-button-right">&#9658;</div>
        </div>';
}
function get_script() : string{
  return '<script type="text/javascript">
    class c_div_stack{
      constructor(){
        this.e_r = document.getElementById(\'weather-right\');
        this.e_l = document.getElementById(\'weather-left\');
        this.x = document.getElementsByClassName(\'weather-item\');
        this.p = document.getElementsByClassName(\'chart-bar\');
        this.a_index = 0;
        this.e_l.onclick = function(e){
          if(this.a_index - 1 < 0)
            return;
          this.x[this.a_index].style.display = "none";
          this.x[--this.a_index].style.display = "block";
          this.update_title();
        }.bind(this);
        this.e_r.onclick = function(e){
          if(this.a_index + 1 >= this.x.length)
            return;
          this.x[this.a_index].style.display = "none";
          this.x[++this.a_index].style.display = "block";
          this.update_title();
        }.bind(this);
        for(let i = 0; i < this.x.length; ++i){
          let r = this.p[i].getAttribute(\'rot\');
          console.log(i, this.x, r);
          this.p[i].style.transform = "rotate(" + r + "deg)";
        }
      }
      update_title(string){
        let dom_ob = this.x[++this.a_index].querySelector("#weather-id-title");
        if(dom_ob == null) return;
        let target = document.getElementById("weather-info-center");
        //this.x[++this.a_index].innerHTML;
        target.innerHTML = dom_ob.innerHTML;
      }
    };
    let weather_table = new c_div_stack();
    </script>';
}
function print_line($string){
  echo '<p>' . $string . '</p>';
}
class block_weather extends block_base {
    const UPDATE_TIMEOUT = 300;
    private $stats_requests = 0;
    private static $g_instance_count = 0;
    private static $g_content_cache = 0;

    private $_cache_lastexecuted = 0;
    private $_cache_lastbuild = 0;
    private function _get_data(){ /* Nothing here yet! */ }
    /**
    * _rebuild_html
    * Fetch and re-cache content
    * @return boolean/stdClass ($this->content)
    */
    private function _rebuild_footer(){
      $mdiff = ((time() - $this->_cache_lastexecuted) / 60);
      $mdifftext = $mdiff > 1440 ? ($mdiff > 1440 * 7 ? 'one week' : 'one day') : 'less than a day';
      $this->content->footer .= '<div class="weather-footer">
                                  <div>Data served via https://openweathermap.org/api</div>';
      //$this->content->footer .= html_writer::tag('div', );.
      $this->content->footer .= html_writer::tag('div', 'last updated: ' . date('g:i:s', $this->_cache_lastexecuted) . ' '. (int)$mdiff . ' minutes ago (' . $mdifftext . ')');
      $this->content->footer .= html_writer::tag('div', 'last rebuilt: ' . (time() - $this->_cache_lastbuild) . ' seconds ago');
      $this->content->footer .= '</div>';
    }
    private function _rebuild_html(){
      global $CFG, $DB;
      echo '_rebuilding_html';
      $ret = NULL;
      try{
        $ret = $DB->get_record('block_weather', array('id'=> '1'));
      }catch(Exception $e){
        return $this->set_error(enum_task_error::FAILED_TO_READ_DB);
      }
      if(
        $ret == NULL || $ret == FALSE
        || !isset($ret->id, $ret->data, $ret->time_last_update, $ret->update_state)
        //|| !is_integer($ret->time_last_update)
      ){
        return $this->set_error(enum_task_error::FAILED_TO_PARSE_DATA);
      }
      $json = json_decode($ret->data, true);
      if($json == NULL || $json == FALSE)
        return $this->set_error(enum_task_error::BAD_RESPONSE);
      $this->_cache_lastexecuted = $ret->time_last_update;
      $this->_cache_lastbuild = time();

      //echo print_r($ret->data);
      $str_location = $json['city']['name'];
      $str_locationcode = $json['city']['country'];
      $int_count = $json['cnt'];
      $this->content->text .= html_writer::tag('div', $str_location . ' /' . $str_locationcode . ' 7 day forecast');
      for($i = 0; $i < $int_count; $i++){
        $element = $json['list'][$i];
        $int_min = $element['temp']['min'];
        $int_max = $element['temp']['max'];

        //Get weather general data
        $array_info =         $element['weather'][0];
        $str_title =          $array_info['main'];
        $str_description =    $array_info['description'];
        $str_fmt_formal =    date('l', $element['dt']);
        $str_fmt_time =      date("Y-m-d", $element['dt']);
        $is_today = (gmdate('d', $element['dt']) == gmdate('d', time()));
        $str_description = ucfirst($str_description);
        $this->content->text .=
          html_writer::start_tag(
            'div',
            array('id' => '0',
                  'class' => 'weather-item ' . ($is_today ? 'weather-today' : '')
                )
          );
        $this->content->text .=
          '<div class="temp-square">
            <div class="temp-square-top temp-max">'. $int_max . '&deg;</div>
            <div class="temp-square-bottom">'. $int_min .'&deg;</div>
            <div class="temp-square-divider temp-min"></div>
          </div>';

        $this->content->text .=
            html_writer::tag('div', $str_fmt_formal, array('class' => 'weather-span')) .
            html_writer::tag(
            'span',
            $str_title) . //. ' ' . $int_min . ' - ' . $int_max
        html_writer::tag('div', $str_description, array('class' => 'weather-info'));
        $this->content->text .= html_writer::end_tag('div');
      }
      return enum_task_error::OK;
    }
    public function data_can_refresh_cache(){
      $bcache = ($this->last_request < (time() - self::UPDATE_TIMEOUT)); //older than 60 * n seconds
      $this->last_request = time();
      return $bcache;
    }
    function __destruct() {
      self::$g_instance_count--;
    }
    /**
    * Set errors and abort
    * This will be returned to the client as error: 'code' in the footnote block
    * Flow is immediately aborted and return a new stdclass footnote
    * @return stdClass ($this->content)
    */
    private function set_error($int_code) : stdClass{
      //$id = new stdClass();//$id->footnote
      $this->content->footer = ' error: (' . $int_code . '), Something went wrong (' . self::$g_instance_count . ')' ;
      return $this->content;
    }

    /* Init will only be called once when loading the page */
    public function init() {
      self::$g_instance_count++;
      $this->_cache_lastexecuted = time();
      global $CFG, $DB;
      //for($x = 0; $x < 5; $x++) echo '<p>.</p>'; //pad page to display raw echo
      $this->last_request = (time() - 60 * 10);   //Set update timer to 10 minutes ago
      $this->title = get_string('weather', 'block_weather');
    }

    /* Config not yet setup! */
    function has_config() {
      return true;
    }
    public function instance_allow_multiple() {
      return true;
    }
    /* Called per instance of plugin on the page */
    public function get_content() {
      $this->stats_requests++;
      if ($this->content == null)
        $this->content = new stdClass;
      $this->content->text    = ' ';
      $this->content->footer  = ' ';
      print_line( $this->_cache_lastbuild );
      if($this->data_can_refresh_cache()) // Looking to persist across the user instance, but for now, It's just going to lock after the first call per instance
        $this->_rebuild_html();
      $this->_rebuild_footer();           //Draw footer
      return $this->content;
    }
    public function specialization() {
      if (isset($this->config)) {
        if (empty($this->config->title)) {
            $this->title = get_string('defaulttitle', 'block_weather');
        } else {
            $this->title = $this->config->title;
        }
        if (empty($this->config->text)) {
            $this->config->text = get_string('defaulttext', 'block_weather');
      }
    }
  }
};
