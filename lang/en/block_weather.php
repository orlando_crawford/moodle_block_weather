<?php
$string['pluginname'] = 'block_weather';
$string['title'] = 'Simple Weather App';
$string['defaulttitle'] = 'Simple weather App';


$string['weather'] = 'Simple Weather App';
$string['weather:addinstance'] = 'Add a weather app';
$string['weather:myaddinstance'] = 'Add Weather App to the My Moodle page';

$string['blockstring'] = '_text_test_blockstring';
$string['task_state'] = '_text_test_blockstring';

$string['headerconfig'] = 'Weather API configuration';
$string['descconfig'] = 'Please configure the global settings';
