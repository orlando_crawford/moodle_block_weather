<?php
$settings->add(
  new admin_setting_heading(
    'headerconfig',
    get_string('headerconfig', 'block_weather'),
    get_string('descconfig', 'block_weather')
  )
);

$reflector = new ReflectionClass('admin_setting_configtext');
echo $reflector->getFileName();

$settings->add(
  new admin_setting_configtext(
    'as_keyconfig',
    'Weather API Key',
    'OpenAPI APP key, this is required',
    ''
  )
);

/**
  * @param string $name
  * @param string $visiblename
  * @param string $description
  * @param mixed $defaultsetting string or array
  * @param mixed $paramtype
  * @param string $cols The number of columns to make the editor
  * @param string $rows The number of rows to make the editor
*/
$settings->add(
  new admin_setting_configtextarea(
    'as_location_input',
    'Location input',
    'Description here',
    'london,daily,week;tokyo,daily,week;',
    '',
    20,
    20
    )
  );
$settings->add(
  new admin_setting_configtext(
    'as_locationconfig',
    'Location',
    'Location code',
    'London'
  )
);
$settings->add(
  new admin_setting_configtext(
    'as_pollrate',
    'Data refresh rate',
    'Location code',
    'London'
  )
);
$settings->add(
  new admin_setting_configcheckbox(
    'as_unitf',
    'Format (Metric/Imperial)',
    'Use imperial instead of metric (Farenheit)',
    0
  )
);
