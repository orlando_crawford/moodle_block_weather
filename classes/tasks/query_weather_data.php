<?php
namespace block_weather\tasks;
class enum_task_error{
  const OK = 0;
  const FAILED_TO_INIT_CURL = 1;
  const FAILED_TO_EXECUTE_CURL = 2;
  const BAD_RESPONSE = 3;
  const REQUEST_BAD_KEY = 4;
  const REQUEST_BAD_ARGS = 5;
  const FAILED_TO_PARSE_DATA = 6;
  const FAILED_TO_WRITE_DB = 7;
  const FAILED_TO_READ_DB = 8;
  const BAD_CONFIG_KEYS = 9;
}
/**
* parse_config_location
* Parse config location table input, and split into an array of locations to query
*
* @return string
*/
function parse_config_location(string $in_string){
  preg_split("([^A-Z,$])+([^A-Z$];)+", $in_string);
}
/**
* get_error_string
* Get and return string corresponding to enum_task_error code
*
* @return string
*/
function get_error_string(int $int_arg) : string{
  if($int_arg < 0 || $int_arg > 8)
    return 'unknown';
    $g_stringtable = array(
      "OK",
      "FAILED_TO_INIT_CURL",
      "FAILED_TO_EXECUTE_CURL",
      "BAD_RESPONSE",
      "REQUEST_BAD_KEY",
      "REQUEST_BAD_ARGS",
      "FAILED_TO_PARSE_DATA",
      "FAILED_TO_WRITE_DB",
      "FAILED_TO_READ_DB",
      "BAD_CONFIG_KEYS"
    );
  return $g_stringtable[$int_arg];
}
class query_weather_data extends \core\task\scheduled_task {
    static  $stat_runcount = 0;
    private $time_start = 0;
    private $time_end = 0;
    function __construct(){}
    function set_error(int $int_arg) : int{
      /* Nothing, just gets the point across*/
      cli_writeln('[weather_app] fault: ' . get_error_string($int_arg) . ' code (' . $int_arg . ')');
      return $int_arg;
    }
    /**
    * Get a descriptive name for this task (shown to admins).
    *
    * @return string
    */
    public function get_name() {
        // Shown in admin screens
        //return get_string('task_1', 'block_weather');
        return 'Server weather data fetcher';
    }
    /**
    * Do the job.
    * Throw exceptions on errors (the job will be retried).
    * @return void
    */
    public function execute() {
      cli_writeln('[weather_app] -> execute');
      global $CFG, $DB;
      self::$stat_runcount++;
      $this->time_start = time();

      // Open CURL
      $ch = curl_init();
      if(!$ch)
        return $this->set_error(enum_task_error::FAILED_TO_INIT_CURL);

      // set URL and other appropriate options
      curl_setopt($ch, CURLOPT_URL, "api.openweathermap.org/data/2.5/forecast/daily?q=London&mode=json&units=metric&cnt=7&APPID=bd5e378503939ddaee76f12ad7a97608");
      //curl_setopt($ch, CURLOPT_URL, "localhost");
      curl_setopt($ch, CURLOPT_HEADER, 0);

      // Disable SSL verification
      curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
      $result = curl_exec($ch); // grab URL and pass it to the browser
      if($result === FALSE)
        return $this->set_error(enum_task_error::FAILED_TO_EXECUTE_CURL);

      $httpcode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
      switch($httpcode){
        case 200: break;
        //case 404: break;
        default:
          //Unexpected!
          return $this->set_error(enum_task_error::BAD_RESPONSE);
          break;
      }
      curl_close($ch); // close cURL resource, and free up system resources
      cli_writeln( '[weather_app] http response: ' . $httpcode);
      // Parse response
      $json = json_decode($result, true);
      if($json == NULL || $json == FALSE || $json['cnt'] == null || $json['list'] == null
      || !is_integer($json['cnt']) || !is_array($json['list']))
        return $this->set_error(enum_task_error::BAD_RESPONSE);
      $ret = NULL;
      try{
         $ret = $DB->get_record_sql("SELECT id from mdl_block_weather ORDER BY id DESC LIMIT 1;");
      }catch(Exeption $e){
        return $this->set_error(enum_task_error::FAILED_TO_READ_DB);
      }
      $curl_time_end = time();
      // cli_writeln( '[weather_app] debug: ' . print_r($json));
      { //Push scope
        $target= new \stdClass;
        $target->id = 1;
        $target->data = $result;
        $target->time_last_update = time();
        $target->update_state = 1;
        $target->update_remarks = 0x1 | 0x2;
        $target->execution_time = $curl_time_end - $this->time_start; //Seconds

        $ret = NULL;
        $ret = $DB->update_record('block_weather', $target, false);
        if(!$ret)
          return $this->set_error(enum_task_error::FAILED_TO_WRITE_DB);
      }
      //api.openweathermap.org/data/2.5/forecast/daily?q=London&mode=json&units=metric&cnt=7&APPID=bd5e378503939ddaee76f12ad7a97608'
      //json_decode ( string $json [, bool $assoc = FALSE [, int $depth = 512 [, int $options = 0 ]]] )
      $this->time_end = time();
      $time_delta = $this->time_end - $this->time_start; //seconds
      //$time_delta * 60;
      cli_writeln( '[weather_app -> cron] Execution successful, took: ' . $time_delta . ' seconds' );
    }
}
